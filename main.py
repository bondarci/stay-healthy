#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Импортируем библиотеку pygame
import pygame
import pyganim
import sys
from pygame import *
from player import *
from menu import *
from blocks import *
from monsters import *
from menu import *

# Объявляем переменные
WIN_WIDTH = 1000  # Ширина создаваемого окна
WIN_HEIGHT = 640  # Высота
DISPLAY = (WIN_WIDTH, WIN_HEIGHT)  # Группируем ширину и высоту в одну переменную
BACKGROUND_COLOR = "#000000"

FILE_DIR = os.path.dirname(__file__)


class Camera(object):
    def __init__(self, camera_func, width, height):
        self.camera_func = camera_func
        self.state = Rect(0, 0, width, height)

    def apply(self, target):
        return target.rect.move(self.state.topleft)

    def update(self, target):
        if not target.drunk:
            self.state = self.camera_func(self.state, target.rect)


def camera_configure(camera, target_rect):
    l, t, _, _ = target_rect
    _, _, w, h = camera
    l, t = -l + WIN_WIDTH / 2, -t + WIN_HEIGHT / 2

    l = min(0, l)  # Не движемся дальше левой границы
    l = max(-(camera.width - WIN_WIDTH), l)  # Не движемся дальше правой границы
    t = max(-(camera.height - WIN_HEIGHT), t)  # Не движемся дальше нижней границы
    t = min(0, t)  # Не движемся дальше верхней границы

    return Rect(l, t, w, h)


def loadLevel(levelNumber):  # определение функции (оно одно на весь код)

    global playerX, playerY  # объявляем глобальные переменные, это координаты героя
    global level, platforms

    # очищаем предидущий уровень, если он был загружен ,а также удаляем все сущности с массивов
    level = []
    platforms = []
    entities.empty()
    animatedEntities.empty()

    levelFile = open('%s/levels/{0}.txt'.format(levelNumber) % FILE_DIR)
    line = " "
    commands = []
    while line[0] != "/":  # пока не нашли символ завершения файла
        line = levelFile.readline()  # считываем построчно
        if line[0] == "[":  # если нашли символ начала уровня
            while line[0] != "]":  # то, пока не нашли символ конца уровня
                line = levelFile.readline()  # считываем построчно уровень
                if line[0] != "]":  # и если нет символа конца уровня
                    endLine = line.find("|")  # то ищем символ конца строки
                    level.append(line[0: endLine])  # и добавляем в уровень строку от начала до символа "|"

        if line[0] != "":  # если строка не пустая
            commands = line.split()  # разбиваем ее на отдельные команды
            if len(commands) > 1:  # если количество команд > 1, то ищем эти команды
                if commands[0] == "player":  # если первая команда - player
                    playerX = int(commands[1])  # то записываем координаты героя
                    playerY = int(commands[2])
                if commands[0] == "portal":  # если первая команда portal, то создаем портал
                    tp = BlockTeleport(int(commands[1]), int(commands[2]), int(commands[3]), int(commands[4]))
                    entities.add(tp)
                    platforms.append(tp)
                    animatedEntities.add(tp)
                if commands[0] == "monster":  # если первая команда monster, то создаем монстра
                    mn = Monster(int(commands[1]), int(commands[2]), int(commands[3]), int(commands[4]),
                                 int(commands[5]), int(commands[6]), int(commands[7]))
                    entities.add(mn)
                    platforms.append(mn)
                    monsters.add(mn)
                if commands[0] == "beer":  # если первая команда monster, то создаем монстра
                    br = Beer(int(commands[1]), int(commands[2]), int(commands[3]), int(commands[4]), int(commands[5]),
                              int(commands[6]))
                    entities.add(br)
                    platforms.append(br)
                    monsters.add(br)

                if commands[0] == "gantelia":  # если первая команда monster, то создаем монстра
                    ga = Gantelia(int(commands[1]), int(commands[2]), int(commands[3]), int(commands[4]),int(commands[5]),
                                 int(commands[6]))
                    entities.add(ga)
                    platforms.append(ga)
                    monsters.add(ga)

                if commands[0] == "sigarette":  # если первая команда monster, то создаем монстра
                    si = Sigarette(int(commands[1]), int(commands[2]), int(commands[3]), int(commands[4]),int(commands[5]),
                                 int(commands[6]))
                    entities.add(si)
                    platforms.append(si)
                    monsters.add(si)

                if commands[0] == "pc":
                    pc = PC(int(commands[1]), int(commands[2]), int(commands[3]), int(commands[4]),int(commands[5]),
                                 int(commands[6]))
                    entities.add(pc)
                    platforms.append(pc)
                    monsters.add(pc)

                if commands[0] == "boss":  # если первая команда boss, то создаем босса
                    bs = Boss(int(commands[1]), int(commands[2]), int(commands[3]), int(commands[4]), int(commands[5]),
                              int(commands[6]), int(commands[7]), int(commands[8]))
                    entities.add(bs)
                    platforms.append(bs)
                    monsters.add(bs)


    createLevel()  # вызов функции createLevel


def createLevel():  # определение(написание того , что она делает) функции createLevel

    global hero, camera

    hero = Player(playerX, 500)  # создаем героя по (x,y) координатам
    entities.add(hero)

    x = y = 0  # координаты
    for row in level:  # вся строка
        for col in row:  # каждый символ
            if col == "-":
                pf = Platform(x, y)
                entities.add(pf)
                platforms.append(pf)
            if col == "*":
                bd = BlockDie(x, y)
                entities.add(bd)
                platforms.append(bd)
            if col == "A":
                ap = Apple(x, y)
                entities.add(ap)
                platforms.append(ap)
            if col == "M":
                mh = Teleport(x, y)
                entities.add(mh)
                platforms.append(mh)
            if col == "H":
                ho = House(x, y)
                entities.add(ho)
                platforms.append(ho)
            if col == "T":
                tr = Tree(x, y)
                entities.add(tr)
                platforms.append(tr)
            if col == "W":
                wl = Wall(x, y)
                entities.add(wl)
                platforms.append(wl)
            if col == "B":
                be = Bed(x, y)
                entities.add(be)
                platforms.append(be)

            x += PLATFORM_WIDTH  # блоки платформы ставятся на ширине блоков
        y += PLATFORM_HEIGHT  # то же самое и с высотой
        x = 0  # на каждой новой строчке начинаем с нуля

    total_level_width = len(level[0]) * PLATFORM_WIDTH  # Высчитываем фактическую ширину уровня
    total_level_height = len(level) * PLATFORM_HEIGHT  # высоту

    camera = Camera(camera_configure, total_level_width, total_level_height)


def main():
    menu()
    timer = pygame.time.Clock()
    left = right = False  # инициализируем наши начальные условия (стоим на месте, игра не закончена , уровень первый)
    up = False
    running = False
    gameOver = False
    levelNumber = 1


    pygame.init()  # Инициация PyGame, обязательная строчка
    myfont = pygame.font.Font("Fonts/Adigiana_2.ttf", 50)
    screen = pygame.display.set_mode(DISPLAY)  # Создаем окошко
    pygame.display.set_caption("Stay Healthy")  # Пишем в шапку
    background_image = pygame.image.load("Pictures/Fon{0}.jpg".format(levelNumber))

    loadLevel(levelNumber)  # вызов функции (вызовов может быть сколько угодно много)
    hero.score = 0

    while not gameOver:  # Основной цикл программы
       # timer.tick(60)
        for e in pygame.event.get():  # Обрабатываем события
            if e.type == QUIT:
                os._exit(0), "QUIT"
            if e.type == KEYDOWN and e.key == K_RIGHT:
                right = True

            if e.type == KEYUP and e.key == K_RIGHT:
                right = False


        screen.blit(background_image, (0, 0))  # Каждую итерацию необходимо всё перерисовывать

        animatedEntities.update()  # показываеaм анимацию
        monsters.update(platforms)  # передвигаем всех монстров
        camera.update(hero)  # центризируем камеру относительно персонажа
        hero.update(left, right, up, running, platforms, entities, levelNumber)  # передвижение
        for e in entities:
            if e.visible:
                screen.blit(e.image, camera.apply(e))

        if levelNumber == 1:
            scoreText = myfont.render("Calories Burned : {0}".format(hero.score), 6, (80, 50,30))
            screen.blit(scoreText, (250, 100))

        if levelNumber == 2:
            scoreText = myfont.render("Strength : {0}".format(hero.strength), 1, (0, 0, 255))
            screen.blit(scoreText, (250, 100))

        if levelNumber == 3:
            scoreText = myfont.render("Tiredness : {0}".format(hero.tired), 1, (0, 0, 255))
            screen.blit(scoreText, (100, 450))




        pygame.display.update()  #обновление и вывод всех изменений на экран

        if hero.winner:  # если становимся победителем - обнуляем счет и меняем уровень

            hero.winner = False
            hero.score = 0
            if levelNumber != 3:
                levelNumber += 1  # увеличиваем уровень на 1
                background_image = pygame.image.load("Pictures/Fon{0}.jpg".format(levelNumber))
            loadLevel(levelNumber)  # вызываем функцию loadLevel

    os._exit(0), "QUIT"


level = []
entities = pygame.sprite.Group()  # Все объекты
animatedEntities = pygame.sprite.Group()  # все анимированные объекты, за исключением героя
monsters = pygame.sprite.Group()  # Все передвигающиеся объекты
platforms = []  # то, во что мы будем врезаться или опираться
if __name__ == "__main__":
    main()
