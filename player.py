#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pygame import *
import pyganim
import os
import blocks
import monsters
import pygame

MOVE_SPEED = 7
MOVE_EXTRA_SPEED = 2.5 # ускорение
WIDTH = 32
HEIGHT = 64
COLOR =  "#888888"
JUMP_POWER = 10
ANIMATION_DELAY = 0.1 # скорость смены кадров
ANIMATION_SUPER_SPEED_DELAY = 0.1 # скорость смены кадров при ускорении
ICON_DIR = os.path.dirname(__file__) #  Полный путь к каталогу с файлами

ANIMATION_RIGHT = [('%s/Hero/r1.png' % ICON_DIR),
            ('%s/Hero/r2.png' % ICON_DIR),
            ('%s/Hero/r3.png' % ICON_DIR),
            ('%s/Hero/r4.png' % ICON_DIR),
            ('%s/Hero/r5.png' % ICON_DIR)]
ANIMATION_RIGHT_FAT = [('%s/Hero/r1_fat.png' % ICON_DIR),
            ('%s/Hero/r2_fat.png' % ICON_DIR),
            ('%s/Hero/r3_fat.png' % ICON_DIR),
            ('%s/Hero/r4_fat.png' % ICON_DIR),
            ('%s/Hero/r5_fat.png' % ICON_DIR)]
ANIMATION_RIGHT_FAT_VERY = [('%s/Hero/r1_fat_very.png' % ICON_DIR),
            ('%s/Hero/r2_fat_very.png' % ICON_DIR),
            ('%s/Hero/r3_fat_very.png' % ICON_DIR),
            ('%s/Hero/r4_fat_very.png' % ICON_DIR),
            ('%s/Hero/r5_fat_very.png' % ICON_DIR)]

ANIMATION_STAY = [('%s/Hero/0.png' % ICON_DIR, 0.1)]
ANIMATION_STAY_FAT = [('%s/Hero/0_fat.png' % ICON_DIR, 0.1)]
ANIMATION_STAY_FAT_VERY = [('%s/Hero/0_fat_very.png' % ICON_DIR, 0.1)]
ANIMATION_SLEEP = [('%s/Hero/sleep/sleep1.png' % ICON_DIR),
                   ('%s/Hero/sleep/sleep2.png' % ICON_DIR),
                   ('%s/Hero/sleep/sleep3.png' % ICON_DIR),
                   ('%s/Hero/sleep/sleep4.png' % ICON_DIR)]

class Player(sprite.Sprite):
    def __init__(self, x, y):
        sprite.Sprite.__init__(self)
        self.xvel = 0   #скорость перемещения. 0 - стоять на месте
        self.startX = 50 # Начальная позиция Х, пригодится когда будем переигрывать уровень
        self.startY = 500
        self.yvel = 0 # скорость вертикального перемещения
        self.onGround = False # На земле ли я?
        self.image = Surface((WIDTH,HEIGHT))
        self.image.fill(Color(COLOR))
        self.rect = Rect(x, y, WIDTH, HEIGHT) # прямоугольный объект
        self.image.set_colorkey(Color(COLOR)) # делаем фон прозрачным
#        Анимация движения вправо
        self.applyAnimation(ANIMATION_RIGHT, ANIMATION_STAY)
#        Анимация движения влево        
        boltAnim = []
        boltAnimSuperSpeed = [] 

        self.winner = False
        self.score = 0
        self.tired = 0
        self.weight = 10
        self.visible = True
        self.drunk = False
        self.drunkCounter = 0
        self.drunkDirection = 5
        self.strength = 0
        self.threshold = 20
        self.speed = MOVE_SPEED
        self.sleepingTime = 0

        
    def applyAnimation(self , animation, animation_stay):
        boltAnim = []
        boltAnimSuperSpeed = []
        for anim in animation:
            boltAnim.append((anim, ANIMATION_DELAY))
            boltAnimSuperSpeed.append((anim, ANIMATION_SUPER_SPEED_DELAY))
        self.boltAnimRight = pyganim.PygAnimation(boltAnim)
        self.boltAnimRight.play()

        self.boltAnimStay = pyganim.PygAnimation(animation_stay)
        self.boltAnimStay.play()
        self.boltAnimStay.blit(self.image, (0, 0)) # По-умолчанию, стоим

    def ApplyAnimationSleep(self , animation):

        self.image = Surface((WIDTH + 32, HEIGHT - 25))

        boltAnim = []
        for anim in animation:
            boltAnim.append((anim, ANIMATION_DELAY))

        self.boltAnimStay = pyganim.PygAnimation(boltAnim)
        self.boltAnimStay.play()
        self.boltAnimStay.blit(self.image, (0, 0))  # По-умолчанию, стоим
        self.sleepingTime = 50

    def update(self, left, right, up, running, platforms, entities, levelNumber):

        if self.sleepingTime > 0 :
                self.boltAnimStay.blit(self.image,(0,0))
                self.sleepingTime -= 1
                if self.sleepingTime == 1:
                    self.image = Surface((WIDTH, HEIGHT))
                    self.image.fill(Color(COLOR))
                    self.image.set_colorkey(Color(COLOR))  # делаем фон прозрачным
                    self.applyAnimation(ANIMATION_RIGHT, ANIMATION_STAY)
                return

        if self.drunk:
            self.drunkwalk(platforms)
        if up:
            if self.onGround:  # прыгаем, только когда можем оттолкнуться от земли
                self.yvel = -JUMP_POWER
                self.image.fill(Color(COLOR))
                self.boltAnimJump.blit(self.image, (0, 0))

        if right:
            self.xvel = self.speed # Право = x + n
            self.score += 1

            if levelNumber == 3:
                self.tired += 1
                #print (self.tired)
                if self.tired >= 100:
                    print("dying")
                    self.die(entities)

            self.image.fill(Color(COLOR))
            if running:
                self.xvel+=MOVE_EXTRA_SPEED
                if not up:
                    self.boltAnimRightSuperSpeed.blit(self.image, (0, 0))
            else:
                if not up:
                    self.boltAnimRight.blit(self.image, (0, 0))


        if not(left or right): # стоим, когда нет указаний идти
            if not self.drunk:
                self.xvel = 0
            if not up:
                self.image.fill(Color(COLOR))
                self.boltAnimStay.blit(self.image, (0, 0))

            
        self.onGround = False; # Мы не знаем, когда мы на земле((   
        self.rect.y += self.yvel
        self.collide(0, self.yvel, platforms, entities)

        self.rect.x += self.xvel # переносим свои положweighttextение на xvel
        self.collide(self.xvel, 0, platforms, entities)

    def drunkwalk(self, platforms):

        if self.drunkCounter > 0:
            self.rect.y += self.drunkDirection
            self.rect.x += 2
            self.drunkCounter -= 1

            for p in platforms:
                if sprite.collide_rect(self, p) and self != p and isinstance(p,blocks.Platform):  # если с чем-то или кем-то столкнулись
                    self.drunkDirection = -self.drunkDirection # если наткнулись на платформу - поворачиваем в обратную сторону
                    break
        else :
            self.rect.y = self.startY
            self.drunk = False
   
    def collide(self, xvel, yvel, platforms, entities):
        for p in platforms:
            if sprite.collide_rect(self, p): # если есть пересечение платформы с игроком
                if isinstance(p, blocks.BlockDie) or isinstance(p, monsters.Monster):
                    if p.visible:
                       self.weight += 30
                       #print (self.weight)
                       p.visible = False
                       if self.weight >= 100:
                          self.die(entities)
                       elif self.weight >= 20 and self.weight <=40:
                           print("fat")
                           self.applyAnimation(ANIMATION_RIGHT_FAT, ANIMATION_STAY_FAT)
                       elif self.weight >40:
                           print ("very fat")
                           self.applyAnimation(ANIMATION_RIGHT_FAT_VERY, ANIMATION_STAY_FAT_VERY)
                elif isinstance(p, blocks.Apple):
                       if p.visible:
                           self.weight -= 5
                           p.visible = False

                elif isinstance(p, blocks.Bed):
                       if p.visible:
                           self.ApplyAnimationSleep(ANIMATION_SLEEP)
                           self.tired = 0
                           p.visible = False

                elif isinstance(p, monsters.Beer):
                       if p.visible:
                           p.visible = False
                           self.drunk = True
                           self.drunkCounter = 35

                elif isinstance(p, monsters.Gantelia):
                    if p.visible:
                        self.strength += 10
                        p.visible = False

                elif isinstance(p, monsters.Sigarette):
                    if p.visible:
                        if (self.speed <= 2):
                                self.speed = 1
                        else:
                            self.speed -= 5

                        p.visible = False

                elif isinstance(p, monsters.PC):
                    if p.visible:
                        self.tired += 30;
                        p.visible = False

                elif isinstance(p, blocks.Wall):
                    if p.visible:
                        if self.strength >= self.threshold:
                            p.visible = False
                            self.threshold += 20
                        else:
                            self.xvel =- MOVE_SPEED


                elif isinstance(p, blocks.Teleport):
                       self.winner = True
                elif isinstance(p, monsters.Boss):
                    if self.CaloresBurned >= 5:
                       self.teleporting(p.teleportX, p.teleportY)
                    else:
                        self.die(entities)
                else:
                    if xvel > 0:                      # если движется вправо
                        self.rect.right = p.rect.left # то не движется вправо

                    if xvel < 0:                      # если движется влево
                        self.rect.left = p.rect.right # то не движется влево

                    if yvel > 0:                      # если падает вниз
                        self.rect.bottom = p.rect.top # то не падает вниз
                        self.onGround = True          # и становится на что-то твердое
                        self.yvel = 0                 # и энергия падения пропадает

                    if yvel < 0:                      # если движется вверх
                        self.rect.top = p.rect.bottom # то не движется вверх
                        self.yvel = 0                 # и энергия прыжка пропадает

    def teleporting(self, goX, goY):
        self.rect.x = goX
        self.rect.y = goY
        
    def die(self, entities):
        self.weight = 10
        self.score = 0
        self.applyAnimation(ANIMATION_RIGHT, ANIMATION_STAY)
        self.tired = 0
        for e in entities:
            e.visible = True
            
        self.CaloresBurned = 0
        self.strength = 0
        time.wait(500)
        self.teleporting(self.startX, self.startY) # перемещаемся в начальные координаты
